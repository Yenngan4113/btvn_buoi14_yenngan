document.getElementById("chanle").addEventListener("click", function () {
  var a = (document.getElementById("so_1").value * 1) % 2;
  var b = (document.getElementById("so_2").value * 1) % 2;
  var c = (document.getElementById("so_3").value * 1) % 2;
  var thongBao = document.getElementById("notif");
  if (a === 0 && b === 0 && c === 0) {
    thongBao.innerHTML = `Có 3 số chẵn`;
  } else if (a != 0 && b != 0 && c != 0) {
    thongBao.innerHTML = `Có 3 số lẻ`;
  } else if (
    (a === 0 && b === 0) ||
    (a === 0 && c === 0) ||
    (b === 0 && c === 0)
  ) {
    thongBao.innerHTML = `Có 2 số chẵn, 1 số lẻ`;
  } else {
    thongBao.innerHTML = `Có 2 số lẻ, 1 số chẵn`;
  }
  thongBao.style.color = "red";
});
