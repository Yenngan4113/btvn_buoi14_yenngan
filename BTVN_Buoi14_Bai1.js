document.getElementById("xuat_thu_tu").addEventListener("click", function () {
  var a = document.getElementById("so_thu_nhat").value * 1;
  var b = document.getElementById("so_thu_hai").value * 1;
  var c = document.getElementById("so_thu_ba").value * 1;
  var sapXep = document.getElementById("arrangement");
  if (a >= b && b >= c) {
    sapXep.innerHTML = `Thứ tự tăng dần là ${c} , ${b} , ${a}`;
  } else if (a >= b && a >= c && c >= b) {
    sapXep.innerHTML = `Thứ tự tăng dần là ${b} , ${c} , ${a}`;
  } else if (b >= a && a >= c) {
    sapXep.innerHTML = `Thứ tự tăng dần là ${c} , ${a} , ${b}`;
  } else if (b >= a && b >= c && c >= a) {
    sapXep.innerHTML = `Thứ tự tăng dần là ${a} , ${c} , ${b}`;
  } else if (c >= b && b >= a) {
    sapXep.innerHTML = `Thứ tự tăng dần là ${a} , ${b} , ${c}`;
  } else {
    sapXep.innerHTML = `Thứ tự tăng dần là ${b} , ${a} , ${c}`;
  }
});
